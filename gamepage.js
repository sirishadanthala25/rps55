let userScore = 0;
        let computerScore = 0;

        function play(userMove) {
            const computerMove = generateComputerMove();
            const result = determineWinner(userMove, computerMove);
            updateScore(result);
            displayResult(userMove, computerMove, result);
        }

        function generateComputerMove() {
            const moves = ['rock', 'paper', 'scissors'];
            return moves[Math.floor(Math.random() * 3)];
        }

        function determineWinner(user, computer) {
            if (user === computer) {
                return 'It\'s a tie!';
            } else if (
                (user === 'rock' && computer === 'scissors') ||
                (user === 'paper' && computer === 'rock') ||
                (user === 'scissors' && computer === 'paper')
            ) {
                return 'You win!';
            } else {
                return 'Computer wins!';
            }
        }
function updateScore(result) {
            if (result === 'You win!') {
                userScore++;
            } else if (result === 'Computer wins!') {
                computerScore++;
            }
            document.getElementById('user-score').textContent = `Your Score: ${userScore}`;
            document.getElementById('computer-score').textContent = `Computer Score: ${computerScore}`;
        }

        function resetGame() {
            userScore = 0;
            computerScore = 0;
            document.getElementById('user-score').textContent = `Your Score: 0`;
            document.getElementById('computer-score').textContent = `Computer Score: 0`;
            document.getElementById('result').textContent = '';
            document.getElementById('winner').textContent = '';
        }

        function declareWinner() {
            if (userScore > computerScore) {
                document.getElementById('winner').textContent = 'You win the game!';
            } else if (computerScore > userScore) {
                document.getElementById('winner').textContent = 'Computer wins the game!';
            } else {
                document.getElementById('winner').textContent = 'It\'s a tie!';
            }
        }